urlencode() {
    # urlencode <string>

    old_lc_collate=$LC_COLLATE
    LC_COLLATE=C	
    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:$i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf '%s' "$c" ;;
            *) printf '%%%02X' "'$c" ;;
        esac
    done

    LC_COLLATE=$old_lc_collate
}

echo -n "Username for 'https://gitlab.com':"
read user_name  
echo -n "Password for 'https://$user_name@gitlab.com':"
read -s user_pass

SDK_USER=$(urlencode $user_name)
SDK_PASS=$(urlencode $user_pass)

SDK_URL=https://$SDK_USER:$SDK_PASS@gitlab.com/cdac-vega/vega-sdk.git
TOOLS_URL=https://$SDK_USER:$SDK_PASS@gitlab.com/cdac-vega/vega-tools.git

echo ""

git clone $SDK_URL
git clone $TOOLS_URL

INSTALL_DIR=`pwd`

if [ -d "vega-tools" ]
then
	cd vega-tools
	./setup-env.sh
fi

cd $INSTALL_DIR

if [ -d "vega-sdk" ]
then
	cd vega-sdk
	git checkout aries
	./setup.sh
fi

echo ""
echo "VEGA SDK/Tools Installation completed"

cd examples/uart/hello_world
make upload




